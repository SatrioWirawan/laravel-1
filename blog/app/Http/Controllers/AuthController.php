<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerForm() {
        return view('signupform');
    }
    public function welcome(Request $request){
        $nama = $request["first-name"]; 
        $belakang = $request["last-name"];

        return view('welcome',compact("nama", "belakang"));
    }
}
