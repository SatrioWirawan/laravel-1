<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->

    </head>
    <body>
        <div>
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>
        </div>
        <div>
            <form id="sign-up-form" action="/welcome" method="post">
                @csrf
                <label for="first-name"><p>First Name:</p></label>
                <input type="text" name="first-name" id="first-name">
                <br>
    
                <label for="last-name"><p>Last Name:</p></label>
                <input type="text" name="last-name" id="last-name">
                <br>
    
                <label for="Gender"><p>Gender:</p></label>
                <input type="radio" name="Gender" value="bed" /> A Bed
                <br>
    
                <input type="radio" name="Gender" value="clock" /> A Clock
                <br>
    
                <input type="radio" name="Gender" value="snake" /> A Snake
                <br>
    
                <label for="nationality"><p>Nationality:</p></label>
                <select name="nationality" id="nationality">
                  <option value="Indonesian">Indonesia</option>
                  <option value="Foreigner">Foreigner</option>
                </select>
                <br>
    
                <p>Language Spoken:</p>
                <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
                <label for="language1">Bahasa Indonesia</label><br>
                <input type="checkbox" id="language2" name="language2" value="English">
                <label for="language2">English</label><br>
                <input type="checkbox" id="language3" name="language3" value="Other">
                <label for="language3">Other</label>
                <br>
    
                <label for="bio"><p>Bio:</p></label>
                <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
                <br>
                
                <input type="submit" value="Sign Up">
            </form>
        </div>
    </body>
</html>
